var balance;
var bet;
var trial = 0;
var die1;
var die2;
var capital = new Array();
var maxCapital;
var countAtMax;

function rollADie(){
	return Math.floor((Math.random() * 6) + 1); //+1 is needed to avoid 0 value
}

function checkInputs() {
	balance = parseInt(document.getElementById("initialBalance").value, 10);
	bet = parseInt(document.getElementById("betSize").value, 10);
	if (isNaN(bet) == true || isNaN(balance) == true) {
		alert("Please enter whole numbers data.");
		return false;
	}
	if (bet > balance) {
		alert("You can't bet more than you have.");
		return false;
	} else if (balance < 10 || bet < 1) {
		alert("Check our requirements: minimal capital is 10$, minimal bet is 1$");
		return false;
	} else {
		return true;
	}
}	

function goBankrupt() {
	maxCapital = balance;
	capital.push(maxCapital);
	do {
		die1 = rollADie();
		die2 = rollADie();
			
		if ((die1 + die2 == 7)) {
			balance += bet * 3;
			}
		else {
			balance -= bet;
			}
			capital.push(balance);
			trial++;
		} while (balance >= bet);
		maxCapital = Math.max.apply(null, capital);
		for (var i=0; i < capital.length; i++) {
			if (parseInt(capital[i], 10) == parseInt(maxCapital, 10)) {
				countAtMax = i + 1;
			}
		}
	}

function updateTableContent() {
	document.getElementById("gameStatTable").style.display = "table";
	document.getElementById("startingBet").innerHTML = bet;
	document.getElementById("totalRollsBeforeBroke").innerHTML = trial;
	document.getElementById("highestAmountWon").innerHTML = maxCapital;
	document.getElementById("rollCountAtMax").innerHTML = countAtMax;
}

function updateButtonText() {
	var buttonText = document.getElementById("playButton");
	if (buttonText.firstChild.data == "Play!")
	 {
		buttonText.firstChild.data = "Play again";
	 }
}

function startGame() {
	if(checkInputs() == true) {
		if (document.getElementById("playButton").firstChild.data == "Play!") {
		goBankrupt();
		updateTableContent();
		updateButtonText();
		} else {
			document.getElementById("startingBet").innerHTML = "";
			document.getElementById("totalRollsBeforeBroke").innerHTML = "";
			document.getElementById("highestAmountWon").innerHTML = "";
			document.getElementById("rollCountAtMax").innerHTML = "";
			goBankrupt();
			updateTableContent();
			updateButtonText();
		}
	} 
}	
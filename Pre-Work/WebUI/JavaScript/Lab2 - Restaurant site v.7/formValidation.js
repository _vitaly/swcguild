function checkInputs() {
	if (document.contactUsForm.Name.value == "") {
		alert("Please provide your name.");
		document.contactUsForm.Name.focus();
		return false;
	}
	
	if (document.contactUsForm.Phone.value == "" &&  document.contactUsForm.Email.value == "") {
		alert("Please provide your phone number or email.");
		return false;
	}
	
	if (document.contactUsForm.Reason.value == "OTHER" && document.contactUsForm.AdditionalInfo.value == "") {
		alert("Please provide additional information.");
		document.contactUsForm.AdditionalInfo.focus();
		return false;
	}
	
	if (document.getElementById("Monday").checked == false &&
		document.getElementById("Tuesday").checked == false &&
		document.getElementById("Wednesday").checked == false &&
		document.getElementById("Thursday").checked == false &&
		document.getElementById("Friday").checked == false) {
			alert("Please specify best day/days to contact you.");
			return false;
		}
	return true;
}
